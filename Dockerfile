### DEVELOPMENT
FROM golang:alpine as builder
RUN printf "\033[0;31mBuilding DEVELOPMENT service\033[0m\n"

## Configuring dirs
WORKDIR /build
ADD ./src .

## Starting app with compile daemon
RUN go mod download
RUN go install github.com/githubnemo/CompileDaemon@latest
RUN go install github.com/swaggo/swag/cmd/swag@latest

## Building app
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags '-extldflags "-static"' -o /main cmd/app/main.go
ENTRYPOINT CompileDaemon \
    --exclude-dir="docs" \
    --build="go build -o /main cmd/app/main.go" \
    --command=/main
