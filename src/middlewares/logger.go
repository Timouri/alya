package middlewares

import (
	"bytes"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

// Sentry log all errors to centralized server
func Logger(logSuccess, logSuccessData bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		log := logrus.WithField("url", c.Request.RequestURI)
		blw := &bodyLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw

		statusCode := c.Writer.Status()
		if statusCode > 0 {
			log = log.WithField("status_code", statusCode)
		}

		if len(c.Request.RemoteAddr) > 0 {
			log = log.WithField("IP", c.Request.RemoteAddr)
		}

		if logSuccessData {
			log = log.WithField("response", blw.body.String())
		}

		if c.Errors.Last() != nil {
			log = log.WithError(c.Errors.Last().Err)
			log.Error("Request error")
		} else if logSuccess {
			log.Info("Request success")
		}
	}
}
