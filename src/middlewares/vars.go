package middlewares

import (
	"github.com/gin-gonic/gin"
)

// Vars add variables to context for all requests
func Vars(vars map[string]interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		if len(vars) == 0 {
			c.Next()
		} else {
			for i := range vars {
				c.Set(i, vars[i])
			}
		}
	}
}
