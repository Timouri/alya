package pubapi

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"timouri.pro/alya/common"
	"timouri.pro/alya/models"
	"timouri.pro/alya/repositories"
	"timouri.pro/alya/utils"
)

type QueryCurrenciesResponse []models.CurrencyStamp

// @Summary query all currencies
// @Success 200 {object} 	pubapi.QueryCurrenciesResponse
// @Failure	500	{string}	string	"empty response"
// @Param	created_at	query	string	false	"filter by created at (time in YYYY-MM-DD format)"
// @Param	order_by	query	string	false	"sort order (created_at[.asc (default), .desc])"
// @Param	limit		query	int		false	"pagination	limit (min 1, max 100, default 10)"
// @Param	offset	 	query	int	 	false	"pagination offset (min 0)"
// @Router /currencies [get]
func QueryCurrencies(c *gin.Context) {
	repository := c.Value(common.KeyCurrencyRepository).(repositories.CurrenciesRepository)

	filter := utils.ParseFilter(c, utils.FieldsPack{
		"created_at": struct{}{},
	})

	currencies, err := repository.Query(filter)
	if err != nil {
		c.Error(errors.Wrap(err, "cannot query currencies"))
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, QueryCurrenciesResponse(currencies))
}
