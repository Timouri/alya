package pubapi

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"timouri.pro/alya/common"
	"timouri.pro/alya/models"
	"timouri.pro/alya/repositories"
	"timouri.pro/alya/utils"
)

type QueryBTCPriceResponse []models.BTCPrice

// @Summary query all btc prices
// @Success 200 {object} 	pubapi.QueryBTCPriceResponse
// @Failure	500	{string}	string	"empty response"
// @Param	value	query	string	false	"filter by value"
// @Param	created_at	query	string	false	"filter by created at (time in ISO format)"
// @Param	order_by	query	string	false	"sort order (value[.asc (default), .desc])"
// @Param	limit		query	int		false	"pagination	limit (min 1, max 100, default 10)"
// @Param	offset	 	query	int	 	false	"pagination offset (min 0)"
// @Router /btcusdt [get]
func QueryBTCPrice(c *gin.Context) {
	repository := c.Value(common.KeyBTCPriceRepository).(repositories.BTCPricesRepository)

	filter := utils.ParseFilter(c, utils.FieldsPack{
		"created_at": struct{}{},
		"value":      struct{}{},
	})

	prices, err := repository.Query(filter)
	if err != nil {
		c.Error(errors.Wrap(err, "cannot query btc price"))
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, QueryBTCPriceResponse(prices))
}
