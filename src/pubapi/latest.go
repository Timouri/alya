package pubapi

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"timouri.pro/alya/common"
	"timouri.pro/alya/models"
	"timouri.pro/alya/repositories"
)

type QueryLatestPriceResponse models.ValuteList

// @Summary query latest btc price for currencies
// @Success 200 {object} 	pubapi.QueryLatestPriceResponse
// @Failure	500	{string}	string	"empty response"
// @Router /latest [get]
func QueryLatestPrice(c *gin.Context) {
	btcPriceRepository := c.Value(common.KeyBTCPriceRepository).(repositories.BTCPricesRepository)
	currenciesRepository := c.Value(common.KeyCurrencyRepository).(repositories.CurrenciesRepository)

	price, err := btcPriceRepository.GetLatest()
	if err != nil {
		c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	currencies, err := currenciesRepository.GetTodayStamp()
	if err != nil {
		c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	response := models.ValuteList{}

	// BTC/USDT * USDT/RUB = BTC/RUB
	btcrub := price.Value.Mul(currencies.Valutes["USD"])

	for i := range currencies.Valutes {
		// BTC/RUB / AUD/RUB = BTC * RUB / RUB * AUD = BTC / AUD
		response[i] = btcrub.Div(currencies.Valutes[i])
	}

	c.JSON(http.StatusOK, response)
}

type QueryLatestPriceCurrencyResponse map[string]decimal.Decimal

// @Summary query latest btc price for selected currency
// @Success 200 {object} 	pubapi.QueryLatestPriceCurrencyResponse
// @Failure	500	{string}	string	"empty response"
// @Param	code		path	string	true 	"currency code"
// @Router /latest/{code} [get]
func QueryLatestPriceCurrency(c *gin.Context) {
	code := c.Param("code")
	if len(code) == 0 {
		c.JSON(http.StatusBadRequest, models.NewError("empty_code", "code param not selected, check url"))
		return
	}
	code = strings.ToUpper(code)

	btcPriceRepository := c.Value(common.KeyBTCPriceRepository).(repositories.BTCPricesRepository)
	currenciesRepository := c.Value(common.KeyCurrencyRepository).(repositories.CurrenciesRepository)

	price, err := btcPriceRepository.GetLatest()
	if err != nil {
		c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	currencies, err := currenciesRepository.GetTodayStamp()
	if err != nil {
		c.Error(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	curr, ok := currencies.Valutes[code]
	if !ok {
		c.JSON(http.StatusBadRequest, models.NewError("invalid_code", fmt.Sprintf("code %s not found", code)))
		return
	}

	// BTC/USDT * USDT/RUB = BTC/RUB
	btcrub := price.Value.Mul(currencies.Valutes["USD"])

	c.JSON(http.StatusOK, QueryLatestPriceCurrencyResponse{
		code: btcrub.Div(curr),
	})
}
