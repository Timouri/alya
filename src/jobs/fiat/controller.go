package fiat

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"timouri.pro/alya/repositories"
)

type Controller struct {
	currencyRepository repositories.CurrenciesRepository
	fiatRepository     repositories.FiatRepository
}

func NewController(currencyRepository repositories.CurrenciesRepository, fiatRepository repositories.FiatRepository) *Controller {
	return &Controller{
		currencyRepository: currencyRepository,
		fiatRepository:     fiatRepository,
	}
}

func (c *Controller) StartFetchingCron(ctx context.Context) error {
	cr := cron.New(cron.WithChain(cron.Recover(cron.DefaultLogger)))

	// Every day at 05:00 AM and now
	if _, err := cr.AddFunc("0 5 * * *", c.update); err != nil {
		return errors.Wrap(err, "cannot start cron")
	}

	logrus.Info("Fiat fetching cron started")
	go c.update()
	go cr.Run()
	go func() {
		defer logrus.Info("Fiat fetching cron stopped")
		<-ctx.Done()
		cr.Stop()
	}()

	return nil
}

func (c *Controller) StartFetchingGoroutine(ctx context.Context) {
	go func(ctx context.Context) {
		defer logrus.Info("Fiat fetching stopped")
		logrus.Info("Fiat fetching started")

		// Every 24H from start
		c.update()
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(time.Hour * 24):
				c.update()
			}
		}
	}(ctx)
}

func (c *Controller) update() {
	fetch := func() error {

		valuteList, err := c.fiatRepository.Fetch()
		if err != nil {
			return errors.Wrap(err, "cannot fetch fiats")
		}

		if _, err := c.currencyRepository.SaveCurrenciesStamp(valuteList); err != nil {
			return errors.Wrap(err, "cannot save currencies")
		}

		return nil
	}

	if st, _ := c.currencyRepository.GetTodayStamp(); st != nil {
		logrus.Infof("Fiats already updated. Skipping...")
		return
	}

	if err := fetch(); err != nil {
		logrus.Warnf("cannot update fiats: %v", err)
	} else {
		logrus.Info("currencies successfully updated")
	}
}
