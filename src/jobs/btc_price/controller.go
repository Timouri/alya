package btcprice

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"timouri.pro/alya/repositories"
)

type Controller struct {
	repository       repositories.BTCPricesRepository
	cryptoRepository repositories.CryptoRepository
}

func NewController(repository repositories.BTCPricesRepository, cryptoRepository repositories.CryptoRepository) *Controller {
	return &Controller{
		repository:       repository,
		cryptoRepository: cryptoRepository,
	}
}

func (c *Controller) StartFetching(ctx context.Context) {
	go func() {
		defer logrus.Print("BTC price fetching stopped")
		logrus.Print("BTC price fetching started")

		c.update()
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(time.Second * 10):
				c.update()
			}
		}
	}()
}

func (c *Controller) update() {
	fetch := func() error {
		data, err := c.cryptoRepository.FetchPrice("BTC-USDT")
		if err != nil {
			return errors.Wrap(err, "cannot fetch price")
		}

		if _, err := c.repository.SaveBTCPrice(data); err != nil {
			return errors.Wrap(err, "cannot save price")
		}

		return nil
	}

	if err := fetch(); err != nil {
		logrus.Warnf("cannot update btc price: %v", err)
	} else {
		logrus.Info("btc price successfully updated")
	}

}
