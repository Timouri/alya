package main

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	nested "github.com/antonfisher/nested-logrus-formatter"
	"github.com/davecgh/go-spew/spew"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/ido50/sqlz"
	"github.com/jmoiron/sqlx"
	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
	"timouri.pro/alya/common"
	btcprice "timouri.pro/alya/jobs/btc_price"
	"timouri.pro/alya/jobs/fiat"
	"timouri.pro/alya/middlewares"
	"timouri.pro/alya/pubapi"
	"timouri.pro/alya/repositories"

	_ "github.com/lib/pq"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"timouri.pro/alya/docs"
)

var cfg *common.Configuration

func init() {
	var err error
	cfg, err = common.LoadConfig()

	if err != nil {
		logrus.Fatalf("cannot load config: %v", err)
	}
}

// @title Alya API Document
// @version 1.0
// @description API
// @BasePath /api/v1/alya
// @accept json
// @produce json
// @securitydefinitions.apiKey BearerAuth
// @in header
// @name Authorization
func main() {
	if host := os.Getenv("APP_SWAGGER_HOST"); len(host) > 0 {
		docs.SwaggerInfo.Host = host
	} else {
		docs.SwaggerInfo.Host = "api.example.pro"
	}

	// Configure random
	rand.Seed(time.Now().UnixNano())

	if err := configureGinMode(cfg); err != nil {
		logrus.Fatalf("Cannot configure gin mode: %v", err)
	}

	if err := configureAppLogger(cfg); err != nil {
		logrus.Fatalf("Cannot configure err logger: %v", err)
	}

	logrus.Infof("App config: %v", spew.Sdump(cfg))

	db, dbz, err := connectDB(cfg)
	if err != nil {
		logrus.Fatalf("Cannot connect to database: %v", err)
	}

	logrus.Infof("Database connected")
	defer db.Close()

	pgCurrencyRepository := repositories.NewPgCurrenciesRepository(dbz)
	pgBtcPriceRepository := repositories.NewPgBTCPriceRepository(dbz)

	fiatController := fiat.NewController(pgCurrencyRepository, repositories.NewCBRRepository())
	fiatController.StartFetchingCron(context.Background())

	btcPriceController := btcprice.NewController(pgBtcPriceRepository, repositories.NewKucoinRepository())
	btcPriceController.StartFetching(context.Background())

	engine := gin.New()
	engine.Use(gin.Recovery())
	engine.Use(middlewares.Logger(true, false))
	engine.Use(middlewares.Vars(map[string]interface{}{
		common.KeyConfig:             cfg,
		common.KeyDB:                 db,
		common.KeyDBZ:                dbz,
		common.KeyCurrencyRepository: pgCurrencyRepository,
		common.KeyBTCPriceRepository: pgBtcPriceRepository,
	}))

	engine.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"OPTIONS", "GET", "POST"},
		AllowHeaders:     []string{"Origin", "Content-type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	// API for alya
	v1 := engine.Group("/api/v1/alya")
	{
		v1.GET("/ping", pubapi.Ping)
		v1.GET("/btcusdt", pubapi.QueryBTCPrice)
		v1.GET("/currencies", pubapi.QueryCurrencies)
		v1.GET("/latest", pubapi.QueryLatestPrice)
		v1.GET("/latest/:code", pubapi.QueryLatestPriceCurrency)
	}

	v1.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	logrus.Info("Routers configured")

	srv := &http.Server{
		Handler: engine,
		Addr:    ":" + strconv.Itoa(cfg.Port),
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logrus.Fatalf("Listen err: %v", err)
		}
	}()
	logrus.Infof("Server started at: %d", cfg.Port)

	<-sig
	logrus.Infof("Server stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		if err := db.Close(); err != nil {
			logrus.Infof("Cannot close database connection: %v", err)
		}

		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		logrus.Fatalf("Server Shutdown Failed: %+v", err)
	}
}

func configureGinMode(cfg *common.Configuration) error {
	if !cfg.GinDebug {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}

	return nil
}

func configureAppLogger(cfg *common.Configuration) error {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(colorable.NewColorableStdout())
	logrus.SetFormatter(&nested.Formatter{
		TimestampFormat: "-",
		FieldsOrder:     []string{"component", "api", "endpoint", "function"},
	})

	return nil
}

func connectDB(cfg *common.Configuration) (*sqlx.DB, *sqlz.DB, error) {
	db, err := sqlx.Connect("postgres", fmt.Sprintf(
		"user=%s password=%s port=%d dbname=%s sslmode=%s host=%s application_name=%s",
		cfg.DBUser.Value(), cfg.DBPass.Value(), cfg.DbPort, cfg.DBName.Value(), cfg.DBSslMode.Value(), cfg.DBHost.Value(), cfg.DBAppName.Value(),
	))
	if err != nil {
		return nil, nil, err
	}

	dbz := sqlz.Newx(db)

	return db, dbz, nil
}
