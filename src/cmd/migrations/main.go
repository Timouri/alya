package main

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"
	"github.com/sirupsen/logrus"
	"timouri.pro/alya/common"

	"database/sql"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

var cfg *common.MigrationConfiguration

func init() {
	var err error
	cfg, err = common.LoadMigrationConfig()

	if err != nil {
		logrus.Fatalf("cannot load config: %v", err)
	}
}

func main() {
	logrus.Info("Running configuration")
	logrus.Infof("App config: %v", spew.Sdump(cfg))

	db, err := connectDB(cfg)
	if err != nil {
		logrus.Fatalf("Cannot connect to database: %v", err)
	}

	logrus.Infof("Database connected")
	defer db.Close()

	if err := upMigrateDB(db); err != nil {
		logrus.Fatalf("Cannot up migrate database: %v", err)
	}

	logrus.Info("Migration successfull")
}

func upMigrateDB(db *sql.DB) error {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return err
	}

	m, err := migrate.NewWithDatabaseInstance("file://app/migrations", "postgres", driver)
	if err != nil {
		return err
	}

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		return err
	}

	return nil
}

func connectDB(cfg *common.MigrationConfiguration) (*sql.DB, error) {
	db, err := sql.Open("postgres", fmt.Sprintf(
		"user=%s password=%s port=%d dbname=%s sslmode=%s host=%s application_name=%s",
		cfg.DBUser.Value(), cfg.DBPass.Value(), cfg.DbPort, cfg.DBName.Value(), cfg.DBSslMode.Value(), cfg.DBHost.Value(), cfg.DBAppName.Value(),
	))
	if err != nil {
		return nil, err
	}

	return db, nil
}
