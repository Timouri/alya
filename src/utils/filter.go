package utils

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/ido50/sqlz"
)

const defaultLimit = 10
const defaultOffset = 0

const maxLimit = 100
const minLimit = 1

const minOffset = 0

// Filter for query params
// pagination: ?limit=40&offset=0
// order: ?order_by=name,surname.desc
// filter: ?username=asd OR ?created_at=gt||17.09.2000
type Filter struct {
	limit   int
	offset  int
	orderBy map[Field]OrderDir
	filter  map[Field][]string
}

type Field string

type FieldsPack map[Field]struct{}

func (p FieldsPack) GetField(key string) (Field, bool) {
	f := Field(strings.ToLower(key))
	_, exists := p[f]
	return f, exists
}

type OrderDir string

const (
	ASC  OrderDir = "ASC"
	DESC OrderDir = "DESC"
)

func ParseFilter(c *gin.Context, pack FieldsPack) Filter {
	result := Filter{}

	offset, exists := c.GetQuery("offset")
	if !exists {
		result.offset = defaultOffset
	} else {
		if num, err := strconv.Atoi(offset); err == nil {
			if num < minOffset {
				result.offset = minOffset
			} else {
				result.offset = num
			}
		}
	}

	limit, exists := c.GetQuery("limit")
	if !exists {
		result.limit = defaultLimit
	} else {
		if num, err := strconv.Atoi(limit); err == nil {
			if num < minLimit {
				result.limit = minLimit
			} else if num > maxLimit {
				result.limit = maxLimit
			} else {
				result.limit = num
			}
		}
	}

	result.orderBy = map[Field]OrderDir{}
	if order, exists := c.GetQuery("order_by"); exists {
		orders := strings.Split(order, ",")
		for i := range orders {
			orderPair := strings.Split(orders[i], ".")

			if field, ok := pack.GetField(orderPair[0]); ok {
				if len(orderPair) == 2 {
					dir := OrderDir(strings.ToUpper(orderPair[1]))
					switch dir {
					case ASC:
						result.orderBy[field] = ASC
					case DESC:
						result.orderBy[field] = DESC
					default:
						result.orderBy[field] = ASC
					}
				} else {
					result.orderBy[field] = ASC
				}
			}
		}
	}

	result.filter = map[Field][]string{}
	for i := range pack {
		if filterValue, exists := c.GetQueryArray(string(i)); exists {
			result.filter[i] = filterValue
		}
	}

	return result
}

func (f *Filter) Limit() int64 {
	switch {
	case f.limit > maxLimit:
		return maxLimit
	case f.limit < minLimit:
		return minLimit
	default:
		return int64(f.limit)
	}
}

func (f *Filter) Offset() int64 {
	switch {
	case f.offset < minOffset:
		return minOffset
	default:
		return int64(f.offset)
	}
}

func (f *Filter) OrderBy() []sqlz.SQLStmt {
	stmt := []sqlz.SQLStmt{}

	for i := range f.orderBy {
		switch f.orderBy[i] {
		case ASC:
			stmt = append(stmt, sqlz.Asc(string(i)))
		case DESC:
			stmt = append(stmt, sqlz.Desc(string(i)))
		}
	}

	return stmt
}

const separator = "||"

func (f *Filter) Where(prefix ...string) []sqlz.WhereCondition {
	stmt := []sqlz.WhereCondition{}

	for i := range f.filter {
		field := string(i)
		if len(prefix) > 0 {
			field = fmt.Sprintf("%s.%s", strings.Join(prefix, "."), field)
		}

		exps := f.filter[i]
		for i := range exps {
			exp := exps[i]

			switch {
			case exp == "isnull":
				stmt = append(stmt, sqlz.IsNull(field))
				continue
			case exp == "notnull":
				stmt = append(stmt, sqlz.IsNotNull(field))
				continue
			case strings.Contains(exp, separator):
				parts := strings.Split(exp, separator)
				if len(parts) == 2 {
					if filter := buildFilter(field, parts[0], parts[1]); filter != nil {
						stmt = append(stmt, filter)
					}
				}
				continue
			}

			// equals filter
			stmt = append(stmt, sqlz.Eq(field, exp))
		}

	}

	return stmt
}

func buildFilter(column string, operator string, value interface{}) sqlz.WhereCondition {
	switch operator {
	case "eq":
		return sqlz.Eq(column, value)
	case "ne":
		return sqlz.Ne(column, value)
	case "gt":
		return sqlz.Gt(column, value)
	case "lt":
		return sqlz.Lt(column, value)
	case "gte":
		return sqlz.Gte(column, value)
	case "lte":
		return sqlz.Lte(column, value)
	default:
		return nil
	}
}
