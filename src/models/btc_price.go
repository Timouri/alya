package models

import (
	"time"

	"github.com/shopspring/decimal"
)

type BTCPrice struct {
	ID        int             `json:"id" db:"id"`
	Value     decimal.Decimal `json:"value" db:"value"`
	CreatedAt time.Time       `json:"created_at" db:"created_at"`
}
