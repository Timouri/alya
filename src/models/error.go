package models

// Error defines the response error
type Error struct {
	Error       string `json:"error" example:"some_error"`
	Description string `json:"description" example:"More information about error"`
}

// NewError instance
func NewError(err, description string) *Error {
	return &Error{
		Error:       err,
		Description: description,
	}
}

// InvalidBody on cannot parse request body
func InvalidBody() Error {
	return Error{
		Error:       "invalid_body",
		Description: "cannot get request body",
	}
}
