package models

import (
	"database/sql/driver"
	"encoding/json"
	"errors"

	"github.com/shopspring/decimal"
)

type CurrencyStamp struct {
	ID        int        `json:"id" db:"id"`
	Valutes   ValuteList `json:"valutes" db:"valutes"`
	CreatedAt Date       `json:"created_at" db:"created_at"`
}

type ValuteList map[string]decimal.Decimal

func (l ValuteList) Value() (driver.Value, error) {
	return json.Marshal(l)
}

func (l *ValuteList) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("source is not []byte")
	}

	return json.Unmarshal(b, &l)
}
