package models

import (
	"encoding/json"
	"time"
)

type Date time.Time

const layout = "2006-01-2"

func (m Date) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(m).Format(layout))
}
