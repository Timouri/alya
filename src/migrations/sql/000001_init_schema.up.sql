create domain btc_amount AS numeric(13, 8);

create table btc_prices (
    id serial primary key,
    value btc_amount not null,
    created_at timestamptz not null default now()
);

create table currency_stamps (
    id serial primary key,
    valutes jsonb not null,
    created_at date not null default now()
);