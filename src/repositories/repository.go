package repositories

import (
	"github.com/shopspring/decimal"
	"timouri.pro/alya/models"
	"timouri.pro/alya/utils"
)

type BTCPricesRepository interface {
	GetLatest() (*models.BTCPrice, error)
	Query(filter utils.Filter) ([]models.BTCPrice, error)
	SaveBTCPrice(price decimal.Decimal) (*models.BTCPrice, error)
}

type CryptoRepository interface {
	FetchPrice(symbol string) (decimal.Decimal, error)
}

type CurrenciesRepository interface {
	Query(filter utils.Filter) ([]models.CurrencyStamp, error)
	GetTodayStamp() (*models.CurrencyStamp, error)
	SaveCurrenciesStamp(list models.ValuteList) (*models.CurrencyStamp, error)
}

type FiatRepository interface {
	Fetch() (models.ValuteList, error)
}
