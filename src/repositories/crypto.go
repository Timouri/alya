package repositories

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
)

type KucoinRepository struct{}

func NewKucoinRepository() *KucoinRepository {
	return &KucoinRepository{}
}

func (r *KucoinRepository) FetchPrice(symbol string) (decimal.Decimal, error) {
	resp, err := http.Get(fmt.Sprintf("https://api.kucoin.com/api/v1/market/stats?symbol=%s", symbol))
	if err != nil {
		return decimal.Decimal{}, errors.Wrap(err, "cannot create price request")
	}
	defer resp.Body.Close()

	data := btcPriceFetchModel{}
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return decimal.Decimal{}, errors.Wrap(err, "cannot decode response body")
	}

	dec, err := decimal.NewFromString(data.Data.Last)
	if err != nil {
		return decimal.Decimal{}, nil
	}

	return dec, nil
}

type btcPriceFetchModel struct {
	Code string                 `json:"code"`
	Data btcPriceFetchDataModel `json:"data"`
}

type btcPriceFetchDataModel struct {
	Time             int64  `json:"time"`
	Symbol           string `json:"symbol"`
	Buy              string `json:"buy"`
	Sell             string `json:"sell"`
	ChangeRate       string `json:"changeRate"`
	ChangePrice      string `json:"changePrice"`
	High             string `json:"high"`
	Low              string `json:"low"`
	Vol              string `json:"vol"`
	VolValue         string `json:"volValue"`
	Last             string `json:"last"`
	AveragePrice     string `json:"averagePrice"`
	TakerFeeRate     string `json:"takerFeeRate"`
	MakerFeeRate     string `json:"makerFeeRate"`
	TakerCoefficient string `json:"takerCoefficient"`
	MakerCoefficient string `json:"makerCoefficient"`
}
