package repositories

import (
	"github.com/ido50/sqlz"
	"github.com/shopspring/decimal"
	"timouri.pro/alya/models"
	"timouri.pro/alya/utils"
)

type PgBTCPricesRepository struct {
	dbz *sqlz.DB
}

func NewPgBTCPriceRepository(dbz *sqlz.DB) *PgBTCPricesRepository {
	return &PgBTCPricesRepository{
		dbz: dbz,
	}
}

func (s *PgBTCPricesRepository) GetLatest() (*models.BTCPrice, error) {
	qb := s.dbz.
		Select("*").
		From("btc_prices").
		OrderBy(sqlz.Desc("created_at")).
		Limit(1)

	sql, args := qb.ToSQL(true)

	btcprice := models.BTCPrice{}
	if err := s.dbz.QueryRowx(sql, args...).StructScan(&btcprice); err != nil {
		return nil, err
	}

	return &btcprice, nil
}

func (s *PgBTCPricesRepository) Query(filter utils.Filter) ([]models.BTCPrice, error) {
	qb := s.dbz.
		Select("*").
		From("btc_prices").
		Where(filter.Where()...).
		OrderBy(filter.OrderBy()...).
		Limit(filter.Limit()).
		Offset(filter.Offset())

	sql, args := qb.ToSQL(true)

	prices := []models.BTCPrice{}
	if err := s.dbz.DB.Select(&prices, sql, args...); err != nil {
		return nil, err
	}

	return prices, nil
}

func (s *PgBTCPricesRepository) SaveBTCPrice(price decimal.Decimal) (*models.BTCPrice, error) {
	qb := s.dbz.
		InsertInto("btc_prices").
		Columns("value").
		Values(price).
		Returning("*")

	sql, args := qb.ToSQL(true)

	btcprice := models.BTCPrice{}
	if err := s.dbz.QueryRowx(sql, args...).StructScan(&btcprice); err != nil {
		return nil, err
	}

	return &btcprice, nil
}
