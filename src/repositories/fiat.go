package repositories

import (
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/shopspring/decimal"
	"golang.org/x/text/encoding/charmap"
	"timouri.pro/alya/models"
)

type CBRRepository struct{}

func NewCBRRepository() *CBRRepository {
	return &CBRRepository{}
}

func (r *CBRRepository) Fetch() (models.ValuteList, error) {
	resp, err := http.Get("http://www.cbr.ru/scripts/XML_daily.asp")
	if err != nil {
		return nil, errors.Wrap(err, "cannot request fiats")
	}
	defer resp.Body.Close()

	respStr, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "cannot decode fiat response")
	}

	data := strings.ReplaceAll(string(respStr), ",", ".")

	fiats := FetchFiatModel{}
	decoder := xml.NewDecoder(strings.NewReader(data))
	decoder.CharsetReader = func(charset string, input io.Reader) (io.Reader, error) {
		switch charset {
		case "windows-1251":
			return charmap.Windows1251.NewDecoder().Reader(input), nil
		default:
			return nil, fmt.Errorf("unknown charset: %s", charset)
		}
	}
	if err := decoder.Decode(&fiats); err != nil {
		return nil, errors.Wrap(err, "cannot decode fiats")
	}

	fiatsMap := map[string]decimal.Decimal{}
	for i := range fiats.Valute {
		value := fiats.Valute[i].Value
		nominal := decimal.NewFromInt(int64(fiats.Valute[i].Nominal))

		price := value.Div(nominal)

		fiatsMap[fiats.Valute[i].CharCode] = price
	}

	return models.ValuteList(fiatsMap), nil
}

type FetchFiatModel struct {
	Valute []FetchFiatValute `xml:"Valute"`
}

type FetchFiatValute struct {
	ID       string          `xml:"ID,attr"`
	NumCode  decimal.Decimal `xml:"NumCode"`
	CharCode string          `xml:"CharCode"`
	Nominal  int             `xml:"Nominal"`
	Name     string          `xml:"Name"`
	Value    decimal.Decimal `xml:"Value"`
}
