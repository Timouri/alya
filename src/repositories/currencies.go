package repositories

import (
	"github.com/ido50/sqlz"
	"timouri.pro/alya/models"
	"timouri.pro/alya/utils"
)

type PgCurrenciesRepository struct {
	dbz *sqlz.DB
}

func NewPgCurrenciesRepository(dbz *sqlz.DB) *PgCurrenciesRepository {
	return &PgCurrenciesRepository{
		dbz: dbz,
	}
}

func (s *PgCurrenciesRepository) Query(filter utils.Filter) ([]models.CurrencyStamp, error) {
	qb := s.dbz.
		Select("*").
		From("currency_stamps").
		Where(filter.Where()...).
		OrderBy(filter.OrderBy()...).
		Limit(filter.Limit()).
		Offset(filter.Offset())

	sql, args := qb.ToSQL(true)

	currencies := []models.CurrencyStamp{}
	if err := s.dbz.DB.Select(&currencies, sql, args...); err != nil {
		return nil, err
	}

	return currencies, nil
}

func (s *PgCurrenciesRepository) GetTodayStamp() (*models.CurrencyStamp, error) {
	qb := s.dbz.
		Select("*").
		From("currency_stamps").
		Where(sqlz.Eq("created_at::date", sqlz.Indirect("CURRENT_DATE")))

	sql, args := qb.ToSQL(true)

	stamp := models.CurrencyStamp{}
	if err := s.dbz.QueryRowx(sql, args...).StructScan(&stamp); err != nil {
		return nil, err
	}

	return &stamp, nil
}

func (s *PgCurrenciesRepository) SaveCurrenciesStamp(list models.ValuteList) (*models.CurrencyStamp, error) {
	qb := s.dbz.
		InsertInto("currency_stamps").
		Columns("valutes").
		Values(list).
		Returning("*")

	sql, args := qb.ToSQL(true)

	stamp := models.CurrencyStamp{}
	if err := s.dbz.QueryRowx(sql, args...).StructScan(&stamp); err != nil {
		return nil, err
	}

	return &stamp, nil
}
