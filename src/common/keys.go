package common

const (
	KeyConfig             = "cfg"
	KeyDB                 = "db"
	KeyDBZ                = "dbz"
	KeyCurrencyRepository = "currency_repository"
	KeyBTCPriceRepository = "btc_price_repository"
)
