package common

import (
	"flag"

	"github.com/go-playground/validator/v10"
)

// MigrationConfiguration stores setting values for migration app
type MigrationConfiguration struct {
	DBUser    PrivateField `json:"-" validate:"required"`
	DBPass    PrivateField `json:"-" validate:"required"`
	DBName    PrivateField `json:"-" validate:"required"`
	DbPort    int          `json:"-" validate:"required"`
	DBHost    PrivateField `json:"-" validate:"required"`
	DBSslMode PrivateField `json:"-" validate:"required"`
	DBAppName PrivateField `json:"-" validate:"required"`
}

// LoadConfig loads configuration from the config file
func LoadMigrationConfig() (*MigrationConfiguration, error) {
	dbUser := envOrFlagStr("APP_DB_USER", flag.String("dbUser", "", "database user"))
	dbPass := envOrFlagStr("APP_DB_PASS", flag.String("dbPass", "", "database password"))
	dbName := envOrFlagStr("APP_DB_NAME", flag.String("dbName", "", "database name"))
	dbPort := envOrFlagInt("APP_DB_PORT", flag.Int("dbPort", 0, "database port"))
	dbHost := envOrFlagStr("APP_DB_HOST", flag.String("dbHost", "", "database host"))
	dbSslMode := envOrFlagStr("APP_DB_SSL_MODE", flag.String("dbSslMode", "", "database ssl mode"))
	dbAppName := envOrFlagStr("APP_DB_APP_NAME", flag.String("dbAppName", "", "database application name"))

	flag.Parse()

	cfg := &MigrationConfiguration{
		DBUser:    PrivateField(*dbUser),
		DBPass:    PrivateField(*dbPass),
		DBName:    PrivateField(*dbName),
		DbPort:    *dbPort,
		DBHost:    PrivateField(*dbHost),
		DBAppName: PrivateField(*dbAppName),
		DBSslMode: PrivateField(*dbSslMode),
	}

	err := validator.New().Struct(cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
