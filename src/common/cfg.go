package common

import (
	"flag"
	"os"
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
)

type PrivateField string

func (f PrivateField) String() string {
	return "<HIDDEN>"
}

func (f PrivateField) Value() string {
	return string(f)
}

// Configuration stores setting values
type Configuration struct {
	Port     int `validate:"required"`
	GinDebug bool

	DBUser    PrivateField `json:"-" validate:"required"`
	DBPass    PrivateField `json:"-" validate:"required"`
	DBName    PrivateField `json:"-" validate:"required"`
	DbPort    int          `json:"-" validate:"required"`
	DBHost    PrivateField `json:"-" validate:"required"`
	DBSslMode PrivateField `json:"-" validate:"required"`
	DBAppName PrivateField `json:"-" validate:"required"`
}

// LoadConfig loads configuration from the config environment
// For each parameter you can use environment variable or flag
func LoadConfig() (*Configuration, error) {
	port := envOrFlagInt("APP_PORT", flag.Int("port", 80, "app listen port"))
	ginDebug := envOrFlagBool("APP_GIN_DEBUG", flag.Bool("ginDebug", false, "debug gin library"))

	dbUser := envOrFlagStr("APP_DB_USER", flag.String("dbUser", "", "database user"))
	dbPass := envOrFlagStr("APP_DB_PASS", flag.String("dbPass", "", "database password"))
	dbName := envOrFlagStr("APP_DB_NAME", flag.String("dbName", "", "database name"))
	dbPort := envOrFlagInt("APP_DB_PORT", flag.Int("dbPort", 0, "database port"))
	dbHost := envOrFlagStr("APP_DB_HOST", flag.String("dbHost", "", "database host"))
	dbSslMode := envOrFlagStr("APP_DB_SSL_MODE", flag.String("dbSslMode", "", "database ssl mode"))
	dbAppName := envOrFlagStr("APP_DB_APP_NAME", flag.String("dbAppName", "", "database application name"))

	flag.Parse()

	cfg := &Configuration{
		Port:     *port,
		GinDebug: *ginDebug,

		DBUser:    PrivateField(*dbUser),
		DBPass:    PrivateField(*dbPass),
		DBName:    PrivateField(*dbName),
		DbPort:    *dbPort,
		DBHost:    PrivateField(*dbHost),
		DBAppName: PrivateField(*dbAppName),
		DBSslMode: PrivateField(*dbSslMode),
	}

	err := validator.New().Struct(cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}

func envOrFlagInt(name string, i *int) *int {
	if e, ok := os.LookupEnv(name); ok {
		if r, err := strconv.Atoi(e); err == nil {
			return &r
		} else {
			logrus.Errorf("cannot parse env int %s: %v", name, err)
		}
	}

	return i
}

func envOrFlagStr(name string, i *string) *string {
	if e, ok := os.LookupEnv(name); ok {
		return &e
	}

	return i
}

func envOrFlagBool(name string, i *bool) *bool {
	if e, ok := os.LookupEnv(name); ok {
		if r, err := strconv.ParseBool(e); err == nil {
			return &r
		} else {
			logrus.Errorf("cannot parse env bool %s: %v", name, err)
		}
	}

	return i
}
