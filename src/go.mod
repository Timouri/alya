module timouri.pro/alya

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/appleboy/gin-jwt v2.5.0+incompatible
	github.com/davecgh/go-spew v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgryski/dgoogauth v0.0.0-20190221195224-5a805980a5f3
	github.com/doug-martin/goqu v5.0.0+incompatible
	github.com/flashmob/go-guerrilla v1.6.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-playground/validator/v10 v10.10.1
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/gookit/validate v1.3.1
	github.com/huandu/go-sqlbuilder v1.13.0
	github.com/ido50/sqlz v1.0.2
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.0
	github.com/mattn/go-colorable v0.1.12
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron/v3 v3.0.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/snowzach/rotatefilehook v0.0.0-20220211133110-53752135082d
	github.com/spf13/viper v1.11.0
	github.com/swaggo/gin-swagger v1.4.1
	github.com/swaggo/swag v1.7.9
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0 // indirect
	gopkg.in/guregu/null.v3 v3.5.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
