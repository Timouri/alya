# Тестовое задание: alya

**Автор**: _<rik.20027698@gmail.com>_

## Описание проекта

### Пути

| URL                                             | description  |
| ----------------------------------------------- | ------------ |
| http://localhost                                | Main service |
| http://localhost:8080                           | PGAdmin      |
| http://localhost/api/v1/alya/swagger/index.html | Swagger UI   |

### Приложение

Все пути приложение начинаются с `/api/v1/alya`, что помогает поддерживать легкую интеграцию с микросервисной архитектурой и kubernetes ingress.

**Endpoints**:
| Method | endpoint | description |
|--------|---------------------------------|--------------------------------------------|
| GET | /api/v1/alya/ping | Ping server status |
| GET | /api/v1/alya/btcusdt | Query all BTC/usdt prices history |
| GET | /api/v1/alya/currencies | Query all currencies prices history |
| GET | /api/v1/alya/latest | Get latest BTC price for currencies |
| GET | /api/v1/alya/latest/:code | Get latest BTC price for selected currency |
| GET | /api/v1/alya/swagger/index.html | Swagger UI |

> NOTE: Приложение НЕ реализует POST методы для получения
> списка сущностей с фильтрацией и пагинацией, т.к. это противоречит базовым
> правилам создания REST api.
> <br/>
> Вместо этого используйте GET запрос с параметрами.

## Контейнеризация

| Dockerfile                | description                                         |
| ------------------------- | --------------------------------------------------- |
| Dockerfile                | Dockerfile for development environment              |
| Dockerfile.prod           | Dockerfile for pgadmin. Use multi-stage building    |
| src/migrations/Dockerfile | Dockerfile for migrations. Use multi-stage building |

Для установки порядка запуска сервисов в `docker-compose.yaml`
используется `healthcheck` вместе с `depends_on`.

## Настройка kubernetes

Здесь описаны все шаги для деплоя приложения в кластер kubernetes.

---

### Установка gitlab агента

1. Переключиться в alya namespace

```bash
kubectl config set-context --current --namespace=alya
```

2. Добавить агента в кластер (Infractructure -> Kubernetes clusters -> connect cluster)

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-agent gitlab/gitlab-agent \
    --namespace alya \
    --create-namespace \
    --set config.token=<token> \
    --set config.kasAddress=wss://kas.gitlab.com \
    --set rbac.create=false
```

2. Назначить агенту роль в текущем пространстве имен

```bash
kubectl create rolebinding gitlab-agent-rb \
    --namespace=alya \
    --serviceaccount=alya:gitlab-agent \
    --clusterrole=admin
```

Gitlab agent готов к работе

### Создание registry credentials

1. Переключиться в alya namespace

```bash
kubectl config set-context --current --namespace=alya
```

2. Создать токен доступа (Settings -> Repository -> Deploy tokens)

Указать возможность read_registry

3. Создать секрет с данными

```bash
kubectl create secret docker-registry registry-credentials --docker-server=https://registry.gitlab.com --docker-username=<name> --docker-password=<password>
```

### Подключение СУБД postgresql

1. Переключение в database namespace

```bash
kubectl config set-context --current --namespace=database
```

2. Создание отдельной базы данных

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: createdb
  namespace: database
spec:
  ttlSecondsAfterFinished: 600
  template:
    spec:
      containers:
        - name: createdb
          image: postgres:11-alpine
          command:
            - sh
            - -c
            - >
              echo "${SQLCOMMAND}" | psql --file=- --echo-queries -d "${ACCESS_SVC_CONNSTR}" \
                --set ON_ERROR_STOP=1 \
                --set dbname="${DBNAME}" \
                --set owner="${OWNER}" \
                --set ownerpass="${OWNER_PASSWORD}"
          env:
            - name: DBNAME
              value: alya
            - name: OWNER
              value: alyaUser
            - name: OWNER_PASSWORD
              value: <microservice-db-pass>
            - name: ACCESS_SVC_CONNSTR
              value: host=postgresql user=postgres connect_timeout=3 sslmode=disable
            - name: SQLCOMMAND
              value: |
                SELECT format('CREATE DATABASE %I', :'dbname')
                WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = :'dbname')
                \gexec

                CREATE USER :"owner" WITH ENCRYPTED PASSWORD :'ownerpass';
                GRANT ALL PRIVILEGES ON DATABASE :"dbname" TO :"owner";
            - name: PGPASSWORD
              valueFrom:
                secretKeyRef:
                  name: postgresql
                  key: postgres-password
      restartPolicy: Never
  backoffLimit: 0
```

3. Переключиться в alya namespace

```bash
kubectl config set-context --current --namespace=alya
```

4. Создать секрет с данными

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: database-access
stringData:
  user: alyaUser
  pass: <microservice-db-pass>
  name: alya
  port: "5432"
  host: postgresql.database
```
